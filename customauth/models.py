from django.db import models

# Create your models here.
from django.contrib.auth.models import (
    AbstractUser, UserManager
)


class UserManager1(UserManager):
    def create_user(self, email=None, password=None, countrycode=91, mobile="", first_name=None,
                    last_name=None, role='1', birth_date=None, country=None, city=None, gender=None, is_super=False):
        user_obj = self.model(
            email=self.normalize_email(email),
            first_name=first_name,
            last_name=last_name,
            mobile=mobile,
            role=role,
            countrycode=countrycode,
            birth_date=birth_date,
            country=country,
            city=city,
            gender=gender,
            is_active=True,
            is_superuser=is_super,
            is_staff=is_super
        )

        user_obj.set_password(password)
        user_obj.save(using=self._db)
        return user_obj

    def create_superuser(self, email, password, countrycode=91, mobile="", first_name=None,
                         last_name=None, role='1', birth_date=None, country=None, city=None, gender=None):
        user = self.create_user(email=email, password=password, countrycode=countrycode, mobile=mobile,
                                first_name=first_name,
                                last_name=last_name, role=role, birth_date=birth_date, country=country, city=city,
                                gender=gender, is_super=True)
        return user


class User(AbstractUser):
    ROLE = [
        (0, 'System_admin'),
        (1, 'Tourist'),
        (2, 'Monument'),
    ]
    COUNTRYCODE = [
        (1, "USA"),
        (91, "INDIA")
    ]
    GENDER = [
        ('M', 'Male'),
        ('F', 'Female'),
        ('O', 'Other'),

    ]
    COUNTRY = [
        ('IN', 'India'),
        ('US', 'USA'),
    ]

    countrycode = models.PositiveSmallIntegerField(choices=COUNTRYCODE)
    mobile = models.CharField(max_length=10, blank=False, unique=True)
    email = models.EmailField(max_length=255, unique=True, null=True)
    first_name = models.CharField(max_length=25)
    last_name = models.CharField(max_length=25, null=True)
    role = models.PositiveSmallIntegerField(choices=ROLE, default=1)
    birth_date = models.DateField(null=True)
    date_joined = models.DateTimeField(auto_now_add=True)
    country = models.CharField(max_length=3, null=True, choices=COUNTRY)
    city = models.CharField(max_length=50, null=True)
    gender = models.CharField(max_length=1, choices=GENDER)

    objects = UserManager1()

    def __str__(self):
        return self.mobile

    USERNAME_FIELD = 'mobile'
