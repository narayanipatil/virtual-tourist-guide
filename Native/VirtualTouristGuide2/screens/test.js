import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Logo from './Logo';
import Form from './Form';
import Wallpaper from './Wallpaper';
import ButtonSubmit from './ButtonSubmit';
import SignupSection from './SignupSection';
import NavigationDrawer from '../navigation/NavigationDrawer';
import {
  Scene,
  Router,
  Actions,
} from 'react-native-router-flux';

class Main extends Component {
    constructor(props) {
        super(props);
        state = {
          uname   : '',
          pass: '',
        };
      }

  render() {
    return (
      <Wallpaper>
        <Logo />
        <Form/>
        <SignupSection
        />
        <ButtonSubmit/>
      </Wallpaper>
    );
  }
}

export default class LoginScreen extends Component {
  render() {
    return (
        <Router>
          <Scene>
            
            <Scene key="home" component={NavigationDrawer} />
          </Scene>
        </Router>
    );
  }
}