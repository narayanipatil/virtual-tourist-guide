import * as React from 'react';
import {
  Text,
  View,
  StyleSheet,
  ImageBackground,
  Image,
  TextInput,
  Dimensions,
  KeyboardAvoidingView
} from 'react-native';
import Constants from 'expo-constants';
import serverUrl from '../constants/url';


// or any pure javascript modules available in npm
import CountryPicker, { DARK_THEME } from 'react-native-country-picker-modal';
import AwesomeButtonRick from 'react-native-really-awesome-button/src/themes/rick';

import bgSrc from '../assets/images/wallpaper.png';
import logoImg from '../assets/images/logo.png';

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      mobileNumber: '',
      countryCode: '91',
      selectedCountry: 'IN',
    };
  }

  render() {
    return (
      <View style={styles.container}>
        <ImageBackground style={styles.picture} source={bgSrc}>
          <View style={styles.logoview}>
            <Image source={logoImg} style={styles.logoimage} />
            <Text style={styles.nametext}>TRAVELr</Text>
          </View>
          <KeyboardAvoidingView behavior={'padding'} style={styles.centerWrapper}>
          <View style={styles.centerWrapper}>
            <View style={styles.inputWrapper}>
              <TextInput
                style={styles.input}
                maxLength={10}
                placeholder={'Mobile Number'}
                keyboardAppearance={'dark'}
                autoCorrect={false}
                autoCapitalize={'none'}
                returnKeyType={'done'}
                placeholderTextColor="white"
                underlineColorAndroid="transparent"
                autoFocus={false}
                keyboardType={'phone-pad'}
                dataDetectorTypes={'phoneNumber'}
                onChangeText={text => {
                  this.setState({ mobileNumber: text });
                }}
                value={this.state.mobileNumber}
              />

              <CountryPicker
                withCloseButton={false}
                withFilter
                withEmoji
                containerButtonStyle={styles.inlineCountryPicker}
                onSelect={value => {
                  this.setState({ countryCode: value['callingCode'] });
                  this.setState({ selectedCountry: value['cca2'] });
                }}
                countryCode={this.state.selectedCountry}
                withCallingCode
                theme={DARK_THEME}
                withCallingCodeButton
              />
              <View
                style={{
                  borderLeftWidth: 1,
                  borderLeftColor: 'black',
                  marginHorizontal: (DEVICE_WIDTH - 350) / 2 + 105,
                  height: 30,
                  top: 10,
                  position: 'absolute',
                }}
              />
            </View>

            <View style={styles.nextWrapper}>
              <AwesomeButtonRick
                type="secondary"
                progress
                width={150}
                raiseLevel={10}
                onPress={next => {

                  fetch(serverUrl.toString() + '/checkMobile/', {
                    method: 'POST',
                    headers: new Headers({
                      'Content-Type': 'application/x-www-form-urlencoded',
                    }),
                    body: 'mobile=' + this.state.mobileNumber,
                  }).then((response) => response.json())
                    .then((responseJson) => {
              
                      console.log(responseJson);
                      // if (responseJson.msg === "success") {
                      //   setTimeout(() => {
                      //     this._onGrow();
                      //   }, 2000);
                      //   setTimeout(() => {
                      //     this.setState({ isLoading: false });
                      //     this.buttonAnimated.setValue(0);
                      //     this.growAnimated.setValue(0);
                      //   }, 2300);
                      //   AsyncStorage.setItem('isLoggedIn', 'true');
                      //   AsyncStorage.setItem('userid', responseJson.session_id);
                      //   this.props.pressed();
                      // }
                      // else if (responseJson.msg === 'error') {
                      //   Vibration.vibrate(150);
                      //   setTimeout(() => {
                      //     this._onGrow();
                      //   }, 300);
                      //   setTimeout(() => {
                      //     this.setState({ isLoading: false });
                      //     this.buttonAnimated.setValue(0);
                      //     this.growAnimated.setValue(0);
                      //     ToastAndroid.show('Wrong Credntials', ToastAndroid.SHORT);
                      //   }, 700);
                      // }
              
                    }).catch((error) => {
                      Vibration.vibrate(150);
                      console.log(error + "Server Error or No Internet Connection");
                      // setTimeout(() => {
                      //   this._onGrow();
                      // }, 300);
                      // setTimeout(() => {
                      //   this.setState({ isLoading: false });
                      //   this.buttonAnimated.setValue(0);
                      //   this.growAnimated.setValue(0);
                      //   ToastAndroid.show('Server Error or No Internet Connection', ToastAndroid.SHORT);
                      // }, 700);
                    });
                  console.log(this.state.mobileNumber);
                  next();
                  this.setState({mobileNumber:''})
                }}
                >
                Next
              </AwesomeButtonRick>
            </View>
          </View>
          </KeyboardAvoidingView>
        </ImageBackground>
      </View>
    );
  }
}

const DEVICE_WIDTH = Dimensions.get('window').width;
const DEVICE_HEIGHT = Dimensions.get('window').height;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 0,
  },
  picture: {
    flex: 1,
    width: null,
    height: null,
    justifyContent: 'center',
    resizeMode: 'cover',
  },
  logoview: {
    top: DEVICE_HEIGHT / 10 - 20,
    flex: 0.7,
    alignItems: 'center',
    justifyContent: 'center',
  },
  logoimage: {
    width: 80,
    height: 80,
  },
  nametext: {
    color: 'white',
    backgroundColor: 'transparent',
    marginTop: 20,
    fontFamily: 'pacifico',
    fontSize: 30,
  },
  centerWrapper: {
    flex: 1,
    flexDirection: 'column',
  },
  inputWrapper: {
    top: 70,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
  },
  input: {
    backgroundColor: 'rgba(255, 255, 255, 0.6)',
    width: 350,
    height: 47,
    marginHorizontal: (DEVICE_WIDTH - 350) / 2,
    paddingLeft: 115,
    borderRadius: 30,
    color: '#ffffff',
    position: 'absolute',
  },
  inlineCountryPicker: {
    top: 10,
    marginHorizontal: (DEVICE_WIDTH - 350) / 2 + 15,
  },
  nextWrapper: {
    top: 15,
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
});
