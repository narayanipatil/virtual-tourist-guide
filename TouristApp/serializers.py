from rest_framework import serializers
from .models import ChatForum, Visitors


class ChatForumSerializer(serializers.ModelSerializer):
    class Meta:
        model = ChatForum
        fields = ['id', 'u_id', 'message', 'timestamp']


class VisitorsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Visitors
        fields = ['p_id', 'u_id', 'timestamp']
