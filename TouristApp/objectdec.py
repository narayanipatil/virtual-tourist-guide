import os

import keras
from django.contrib.staticfiles.templatetags.staticfiles import static
from keras.models import Sequential, load_model
from keras.layers import Dense, Dropout, Flatten
from keras.layers import Conv2D, MaxPooling2D
from keras.utils import to_categorical
from keras.preprocessing import image
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from keras.utils import to_categorical
from tqdm import tqdm

from VirtualTouristGuide.settings import BASE_DIR

model = load_model(os.path.join(BASE_DIR, "TouristApp/static/")+"mymodel.h5")
# img = image.load_img("capture(1).png", target_size=(28, 28, 3), grayscale=False)
# img = image.img_to_array(img)
# img = img / 255
# l = [img]
# a = np.array(l)
# print(model.predict_proba(a))
# print(model.predict_classes(a))
